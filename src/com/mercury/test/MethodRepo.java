package com.mercury.test;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class MethodRepo {
	//test comment
	//test comment 2
	WebDriver driver;
	
	public void browserLounch()
	{
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}

	public void maxBrowser() {
		
		driver.manage().window().maximize();
	}
	
	public void murcuryApplounch() {
		
		driver.get("http://newtours.demoaut.com/");
		
	}
	
	public void closeBrowser() {
		
		driver.close();
	}
	
	public void waiteFor(int timespan) throws InterruptedException {
		
		Thread.sleep(timespan);
	}
	
	public void enterUserName(String uname ) {
		
		//WebElement txtuname = driver.findElement(By.name("userName"));
		WebElement txtuname = driver.findElement(By.xpath("//input[@name='userName']"));
		
		txtuname.sendKeys(uname);
	}
	
	public void enterPass() {
		
		//WebElement txtpass = driver.findElement(By.name("password"));
		WebElement txtpass = driver.findElement(By.xpath("//input[@name='password']"));
		txtpass.sendKeys("dasd");
	}
	
	public void signIn() throws AWTException {
		
		//WebElement btnlogin = driver.findElement(By.name("login"));
		//WebElement btnlogin = driver.findElement(By.xpath("//input[@name='login']"));
		//btnlogin.click();
		
		// Next using Robot Class for Login 
		
		Robot r1 = new Robot();
		r1.keyPress(KeyEvent.VK_ENTER);
	}
	
	public void verifyValidLogin() {
		
		String extectedtitle = "Find an Flight: Mercury Tours:";
		String actualtite = driver.getTitle();
		
		System.out.println(actualtite);
		
		if(extectedtitle.equals(actualtite)) {
			
			System.out.println("Success");
		}
		else {
			System.out.println("Fail");
		}

 }
	public void departingFromSelection () throws InterruptedException
	{
		WebElement pickerarrivingin = driver.findElement(By.xpath("//select[@name='fromPort']"));
		Select se = new Select (pickerarrivingin);
		se.selectByVisibleText("London");
		Thread.sleep(3000);
		se.selectByIndex(4);
		Thread.sleep(3000);
		se.selectByValue("Seattle");
	}
	
}

